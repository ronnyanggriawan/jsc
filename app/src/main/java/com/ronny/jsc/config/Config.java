package com.ronny.jsc.config;


import com.ronny.jsc.BuildConfig;

/**
 * Created by Admin on 23/08/2017.
 */

public class Config {
    public static final String URL_JSC = BuildConfig.BASE_URL+"/api";
    public static final String RESULT = "results";
    public static final String EXC = "exc";
    public static final String GENDER = "gender";
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    public static final String PICTURE = "picture";
    public static final String DOB = "dob";
    public static final String LOCATION = "location";
    public static final String STREETS = "street";
    public static final String COORDINATES = "coordinates";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String NUMBER = "number";
    public static final String DATE = "date";
    public static final String FIRST_NAME = "first";
    public static final String LAST_NAME = "last";
    public static final String MEDIUM = "medium";
    public static final String CELL = "cell";
    public static final String PHONE = "phone";
    public static final String PARCELABLE = "PARCELABEL";

}