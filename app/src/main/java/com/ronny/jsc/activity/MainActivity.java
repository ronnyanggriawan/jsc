package com.ronny.jsc.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.ronny.jsc.R;
import com.ronny.jsc.adapter.AdapterEmp;
import com.ronny.jsc.database.DatabaseSqlite;
import com.ronny.jsc.help.Util;
import com.ronny.jsc.model.Employee;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    ArrayList<Employee> arrayListUser = new ArrayList<>();
    private DatabaseSqlite databaseSqlite;
    LinearLayout lLWrpEmpty, lLHome;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Util.setToolbar(this, "Kelola Kontak");
        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        databaseSqlite = new DatabaseSqlite(this);

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        lLWrpEmpty = findViewById(R.id.wraper_empty);
        lLHome = findViewById(R.id.home);
        swipe = findViewById(R.id.swipe_refresh);
        swipe.setOnRefreshListener(this);
        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           getUSer();
                       }
                   }
        );
    }

    private void getUSer() {
        arrayListUser = databaseSqlite.getAllData();
        if (arrayListUser.size() > 0) {
            swipe.setRefreshing(false);
            AdapterEmp adapter = new AdapterEmp(MainActivity.this, arrayListUser);
            recyclerView.setAdapter(adapter);
            recyclerView.setVisibility(View.VISIBLE);
            lLWrpEmpty.setVisibility(View.GONE);
        } else {
            swipe.setRefreshing(false);
            arrayListUser.clear();
            recyclerView.setVisibility(View.GONE);
            Snackbar.make(lLHome, "Data User Tidak Ada!", Snackbar.LENGTH_LONG).show();
            lLWrpEmpty.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_people:
                Intent intent = new Intent(getApplicationContext(),AddPeople.class);
                startActivity(intent);
                return true;
        }

        return false;
    }

    @Override
    public void onRefresh() {
        getUSer();
    }
}