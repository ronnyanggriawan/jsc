package com.ronny.jsc.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.snackbar.Snackbar;
import com.ronny.jsc.R;
import com.ronny.jsc.adapter.AdapterEmp;
import com.ronny.jsc.config.Config;
import com.ronny.jsc.help.Util;
import com.ronny.jsc.model.Employee;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

public class AddPeople extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    LinearLayout lLWrpEmpty, lLHome;
    RecyclerView recyclerView;
    ArrayList<Employee> KEGIATAN;
    SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_people);
        Util.setToolbar(this, "Kelola Kontak");
        KEGIATAN = new ArrayList<>();
        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        lLWrpEmpty = findViewById(R.id.wraper_empty);
        lLHome = findViewById(R.id.home);
        swipe = findViewById(R.id.swipe_refresh);
        swipe.setOnRefreshListener(this);
        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           getKeg();
                       }
                   }
        );
    }

    private void getKeg() {
        KEGIATAN.clear();
        lLWrpEmpty.setVisibility(View.GONE);
        AndroidNetworking.get(Config.URL_JSC)
                .addQueryParameter(Config.RESULT,"5")
                .addQueryParameter(Config.EXC,"login,registered,i")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray result = response.getJSONArray(Config.RESULT);
                            Log.d("AddPeople","AddPeople "+result);
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject c = result.getJSONObject(i);
                                Employee employee = new Employee();

                                JSONObject jsonName = (JSONObject) new JSONTokener(c.getString(Config.NAME)).nextValue();
                                String name = jsonName.getString(Config.FIRST_NAME)+" "+jsonName.getString(Config.LAST_NAME);

                                JSONObject jsonImg = (JSONObject) new JSONTokener(c.getString(Config.PICTURE)).nextValue();
                                String url_img = jsonImg.getString(Config.MEDIUM);

                                JSONObject jsonDOB = (JSONObject) new JSONTokener(c.getString(Config.DOB)).nextValue();
                                String dob = jsonDOB.getString(Config.DATE);

                                JSONObject jsonAddress = (JSONObject) new JSONTokener(c.getString(Config.LOCATION)).nextValue();
                                JSONObject jsonStreet = (JSONObject) new JSONTokener(jsonAddress.getString(Config.STREETS)).nextValue();
                                String address = jsonStreet.getInt(Config.NUMBER)+" "+jsonStreet.getString(Config.NAME);

                                JSONObject jsonLongLat = (JSONObject) new JSONTokener(jsonAddress.getString(Config.COORDINATES)).nextValue();
                                String lotLang = jsonLongLat.getString(Config.LATITUDE)+","+jsonLongLat.getString(Config.LONGITUDE);

                                employee.setGenderEmp(c.getString(Config.GENDER));
                                employee.setNameEmp(name);
                                employee.setImgEmp(url_img);
                                employee.setBornEmp(dob);
                                employee.setEmailEmp(c.getString(Config.EMAIL));
                                employee.setTelpEmp(c.getString(Config.CELL));
                                employee.setPhoneEmp(c.getString(Config.PHONE));
                                employee.setAddressEmp(address);
                                employee.setLongLatEmp(lotLang);


                                KEGIATAN.add(employee);
                            }

                            if (!KEGIATAN.isEmpty()) {
                                AdapterEmp adapter = new AdapterEmp(getApplication(), KEGIATAN);
                                recyclerView.setAdapter(adapter);
                                recyclerView.setVisibility(View.VISIBLE);
                                lLWrpEmpty.setVisibility(View.GONE);
                                swipe.setRefreshing(false);
                            } else {
                                swipe.setRefreshing(false);
                                KEGIATAN.clear();
                                recyclerView.setVisibility(View.GONE);
                                lLWrpEmpty.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.d("muncul", "error " + error.getErrorDetail());
                        String errorkoneksi = error.getErrorDetail();
                        if (errorkoneksi.equals("connectionError")) {
                            Snackbar.make(lLHome, "Data Gagal Ditampilkan. Mohon Cek Koneksi Anda.", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(lLHome, error.getErrorDetail(), Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
    }


    @Override
    public void onRefresh() {
        getKeg();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}