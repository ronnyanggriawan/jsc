package com.ronny.jsc.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Employee implements Parcelable {
    private String idEmp,nameEmp,imgEmp,bornEmp,genderEmp,emailEmp,telpEmp,phoneEmp,addressEmp,longLatEmp;

    public Employee(String nameEmp, String imgEmp, String bornEmp, String genderEmp, String emailEmp, String telpEmp, String phoneEmp, String addressEmp, String longLatEmp) {
        this.nameEmp = nameEmp;
        this.imgEmp = imgEmp;
        this.bornEmp = bornEmp;
        this.genderEmp = genderEmp;
        this.emailEmp = emailEmp;
        this.telpEmp = telpEmp;
        this.phoneEmp = phoneEmp;
        this.addressEmp = addressEmp;
        this.longLatEmp = longLatEmp;
    }

    public String getIdEmp() {
        return idEmp;
    }

    public void setIdEmp(String idEmp) {
        this.idEmp = idEmp;
    }

    public String getNameEmp() {
        return nameEmp;
    }

    public void setNameEmp(String nameEmp) {
        this.nameEmp = nameEmp;
    }

    public String getImgEmp() {
        return imgEmp;
    }

    public void setImgEmp(String imgEmp) {
        this.imgEmp = imgEmp;
    }

    public String getBornEmp() {
        return bornEmp;
    }

    public void setBornEmp(String bornEmp) {
        this.bornEmp = bornEmp;
    }

    public String getGenderEmp() {
        return genderEmp;
    }

    public void setGenderEmp(String genderEmp) {
        this.genderEmp = genderEmp;
    }

    public String getEmailEmp() {
        return emailEmp;
    }

    public void setEmailEmp(String emailEmp) {
        this.emailEmp = emailEmp;
    }

    public String getTelpEmp() {
        return telpEmp;
    }

    public void setTelpEmp(String telpEmp) {
        this.telpEmp = telpEmp;
    }

    public String getPhoneEmp() {
        return phoneEmp;
    }

    public void setPhoneEmp(String phoneEmp) {
        this.phoneEmp = phoneEmp;
    }

    public String getAddressEmp() {
        return addressEmp;
    }

    public void setAddressEmp(String addressEmp) {
        this.addressEmp = addressEmp;
    }

    public String getLongLatEmp() {
        return longLatEmp;
    }

    public void setLongLatEmp(String longLatEmp) {
        this.longLatEmp = longLatEmp;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.idEmp);
        dest.writeString(this.nameEmp);
        dest.writeString(this.imgEmp);
        dest.writeString(this.bornEmp);
        dest.writeString(this.genderEmp);
        dest.writeString(this.emailEmp);
        dest.writeString(this.telpEmp);
        dest.writeString(this.phoneEmp);
        dest.writeString(this.addressEmp);
        dest.writeString(this.longLatEmp);
    }

    public Employee() {
    }

    protected Employee(Parcel in) {
        this.idEmp = in.readString();
        this.nameEmp = in.readString();
        this.imgEmp = in.readString();
        this.bornEmp = in.readString();
        this.genderEmp = in.readString();
        this.emailEmp = in.readString();
        this.telpEmp = in.readString();
        this.phoneEmp = in.readString();
        this.addressEmp = in.readString();
        this.longLatEmp = in.readString();
    }

    public static final Creator<Employee> CREATOR = new Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel source) {
            return new Employee(source);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };
}