package com.ronny.jsc.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ronny.jsc.model.Employee;

import java.util.ArrayList;

public class DatabaseSqlite extends SQLiteOpenHelper {

    public static final String DB_NAME = "K24Test.db";
    public static final String TABLE_EMPLOYEE = "tbl_employee";
    public static final String ID_USER = "id_user";
    public static final String NAMA = "nama";
    public static final String JEN_KEL = "jen_kel";
    public static final String IMG_EMP = "img_emp";
    public static final String TGL_LHR = "tgl_lhr";
    public static final String EMAIL_EMP = "email_emp";
    public static final String TLP_EMP = "tlp_emp";
    public static final String PHONE_EMP = "phone_emp";
    public static final String ALAMAT = "alamat";
    public static final String LAT_LONG = "lat_long";
    public static final String COLUMN_STATUS = "status";
    private static final int DB_VERSION = 1;

    public DatabaseSqlite(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_EMPLOYEE
                + "(" + ID_USER + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAMA + " VARCHAR, "
                + JEN_KEL + " VARCHAR, "
                + IMG_EMP + " VARCHAR, "
                + TGL_LHR + " VARCHAR, "
                + EMAIL_EMP + " VARCHAR, "
                + TLP_EMP + " VARCHAR, "
                + PHONE_EMP + " VARCHAR, "
                + ALAMAT + " VARCHAR, "
                + LAT_LONG + " VARCHAR, "
                + COLUMN_STATUS + " TINYINT);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Persons";
        db.execSQL(sql);
        onCreate(db);
    }

    public boolean addUser(String nama, String jen_kel, String img_emp, String tgl_lhr, String email_emp, String tlp_emp, String phone_emp
                           ,String alamat,String lat_long,int statusdb) {
        SQLiteDatabase db = this.getWritableDatabase();
        String queryValues = "INSERT INTO tbl_employee ( nama,jen_kel,img_emp,tgl_lhr,email_emp,tlp_emp,phone_emp,alamat,lat_long,status)" +
                " VALUES ( '" + nama + "','" + jen_kel + "','" + img_emp + "'" +
                ",'" + tgl_lhr + "','" + email_emp + "','" + tlp_emp + "','" + phone_emp + "','" + alamat + "','" + lat_long + "','" + statusdb + "');";
        Log.d("insert sqlite ", "" + queryValues);
        db.execSQL(queryValues);
        db.close();
        return true;
    }

    public ArrayList<Employee> getAllData() {
        ArrayList<Employee> arrayBiodata = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_EMPLOYEE;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Employee modelBiodata = new Employee();

                modelBiodata.setIdEmp( cursor.getString(0));
                modelBiodata.setNameEmp( cursor.getString(1));
                modelBiodata.setGenderEmp( cursor.getString(2));
                modelBiodata.setImgEmp( cursor.getString(3));
                modelBiodata.setBornEmp(cursor.getString(4));
                modelBiodata.setEmailEmp( cursor.getString(5));
                modelBiodata.setTelpEmp( cursor.getString(6));
                modelBiodata.setPhoneEmp( cursor.getString(7));
                modelBiodata.setAddressEmp( cursor.getString(8));
                modelBiodata.setLongLatEmp( cursor.getString(9));
                arrayBiodata.add(modelBiodata);
            } while (cursor.moveToNext());
        }
        database.close();
        return arrayBiodata;
    }

    public boolean updateNameStatus(String id, String NEWPASSWORD) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(LAT_LONG, NEWPASSWORD);
        db.update(TABLE_EMPLOYEE, contentValues, ID_USER + "=" + id, null);
        db.close();
        return true;
    }

    public Cursor getUnsyncedNames() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_EMPLOYEE + " WHERE " + COLUMN_STATUS + " = 0;";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    public void removeAll() {
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(DatabaseSqlite.TABLE_EMPLOYEE, COLUMN_STATUS + " =" + 1, null);
    }

    public boolean loginUser(String email_emp, String lat_long) {
        String[] columns = {ID_USER};
        SQLiteDatabase db = getReadableDatabase();
        String selection = EMAIL_EMP + "=?" + " and " + LAT_LONG + "=?";
        String[] selectionArgs = {email_emp, lat_long};
        Cursor CekLogin = db.query(TABLE_EMPLOYEE, columns, selection, selectionArgs, null, null, null);
        int countCekLogin = CekLogin.getCount();
        CekLogin.close();

        db.close();

        if (countCekLogin > 0)
            return true;
        else
            return false;
    }

    public boolean cekUser(String email_emp) {
        String[] columns = {ID_USER};
        SQLiteDatabase db = getReadableDatabase();
        String selectionUser = EMAIL_EMP + "=?";
        String[] selectionArgsUser = {email_emp};
        Cursor CekUser = db.query(TABLE_EMPLOYEE, columns, selectionUser, selectionArgsUser, null, null, null);
        int countCekUser = CekUser.getCount();
        CekUser.close();
        db.close();

        if (countCekUser > 0)
            return true;
        else
            return false;
    }
}