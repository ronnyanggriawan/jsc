package com.ronny.jsc.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ronny.jsc.R;
import com.ronny.jsc.activity.DetailEmp;
import com.ronny.jsc.activity.MainActivity;
import com.ronny.jsc.config.Config;
import com.ronny.jsc.database.DatabaseSqlite;
import com.ronny.jsc.model.Employee;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterEmp extends RecyclerView.Adapter<AdapterEmp.Muncul> {

    Context context;
    private ArrayList<Employee> employeeLis;

    public AdapterEmp(Context context, ArrayList<Employee> MDView) {
        this.employeeLis = MDView;
        this.context = context;
    }

    @Override
    public AdapterEmp.Muncul onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null);
        return new AdapterEmp.Muncul(v);
    }

    @Override
    public void onBindViewHolder(final AdapterEmp.Muncul holder, final int position) {
        holder.tvName.setText(employeeLis.get(position).getNameEmp());
        Glide.with(context).load(employeeLis.get(position).getImgEmp()).diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).placeholder(R.drawable.new_person).into(holder.imgEmp);
        final Employee employee = new Employee();

        employee.setNameEmp(employeeLis.get(position).getNameEmp());
        employee.setGenderEmp(employeeLis.get(position).getGenderEmp());
        employee.setImgEmp(employeeLis.get(position).getImgEmp());
        employee.setBornEmp(employeeLis.get(position).getBornEmp());
        employee.setEmailEmp(employeeLis.get(position).getEmailEmp());
        employee.setTelpEmp(employeeLis.get(position).getTelpEmp());
        employee.setPhoneEmp(employeeLis.get(position).getPhoneEmp());
        employee.setAddressEmp(employeeLis.get(position).getAddressEmp());
        employee.setLongLatEmp(employeeLis.get(position).getLongLatEmp());


        holder.lLBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseSqlite databaseSqlite = new DatabaseSqlite(context);
                List<Employee> modelEmp = new ArrayList<>();

                databaseSqlite.addUser(employeeLis.get(position).getNameEmp(), employeeLis.get(position).getGenderEmp()
                        , employeeLis.get(position).getImgEmp(), employeeLis.get(position).getBornEmp()
                        , employeeLis.get(position).getEmailEmp(), employeeLis.get(position).getTelpEmp()
                        , employeeLis.get(position).getPhoneEmp(), employeeLis.get(position).getAddressEmp()
                        ,employeeLis.get(position).getLongLatEmp(),1);
                Employee n = new Employee(employeeLis.get(position).getNameEmp(), employeeLis.get(position).getGenderEmp()
                        , employeeLis.get(position).getImgEmp(), employeeLis.get(position).getBornEmp()
                        , employeeLis.get(position).getEmailEmp(), employeeLis.get(position).getTelpEmp()
                        , employeeLis.get(position).getPhoneEmp(), employeeLis.get(position).getAddressEmp()
                        ,employeeLis.get(position).getLongLatEmp());
                modelEmp.add(n);
                Intent it = new Intent(view.getContext(), MainActivity.class);
                it.putExtra(Config.PARCELABLE, employee);
                view.getContext().startActivity(it);
            }
        });


    }

    @Override
    public int getItemCount() {
        return employeeLis.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class Muncul extends RecyclerView.ViewHolder {
        TextView tvName;
        LinearLayout lLBtn;
        CircleImageView imgEmp;

        public Muncul(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_nama);
            imgEmp = itemView.findViewById(R.id.img_profile);
            lLBtn = itemView.findViewById(R.id.ll_btn);

        }

    }
}